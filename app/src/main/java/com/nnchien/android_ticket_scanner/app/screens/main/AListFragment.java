package com.nnchien.android_ticket_scanner.app.screens.main;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.nnchien.android_ticket_scanner.R;

import com.nnchien.android_ticket_scanner.app.screens.ATrackedFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestViewHolder;
import com.nnchien.android_ticket_scanner.models.FirebaseData;
import com.nnchien.android_ticket_scanner.app.screens.FirebaseDataViewHolder;
import com.nnchien.android_ticket_scanner.models.Guest;

import butterknife.BindView;

public abstract class AListFragment extends ATrackedFragment {

    private static final int CHECK_IN = ItemTouchHelper.LEFT;

    private static final int CHECK_OUT = ItemTouchHelper.RIGHT;

    protected abstract FirebaseRecyclerAdapter<? extends FirebaseData, ? extends FirebaseDataViewHolder> getAdapter(String searchText);

    protected boolean isSupportedSwipeableCheckIn() {
        return false;
    }

    protected boolean isSupportedSwipeableCheckOut() {
        return false;
    }

    protected void onSwipeCheckIn(Guest data) {
        // will be overridden by child fragment
    }

    protected void onSwipeCheckOut(Guest data) {
        // will be overridden by child fragment
    }

    private FirebaseRecyclerAdapter<? extends FirebaseData, ? extends FirebaseDataViewHolder> mAdapter;
    private LinearLayoutManager mManager;
    @BindView(R.id.messages_list) RecyclerView mRecycler;
    private Paint p = new Paint();

    public AListFragment() {
    }

    @Override
    protected View setContentView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_all_guests, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecycler.setHasFixedSize(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        search(null); // load all
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public void search(String searchText) {
        mAdapter = null;
        mAdapter = getAdapter(searchText);
        mRecycler.setAdapter(mAdapter);
        initSwipe();
        mAdapter.notifyDataSetChanged();
    }

    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                if(isSupportedSwipeableCheckIn()) {
                    return makeMovementFlags(0, ItemTouchHelper.LEFT);
                }
                if(isSupportedSwipeableCheckOut()) {
                    return makeMovementFlags(0, ItemTouchHelper.RIGHT);
                }
                return makeMovementFlags(0, ItemTouchHelper.ACTION_STATE_IDLE);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                if(viewHolder instanceof GuestViewHolder) {
                    Guest data = (Guest)((GuestViewHolder) viewHolder).getData();
                    switch (direction) {
                        case CHECK_IN:
                            onSwipeCheckIn(data);
                            break;
                        case CHECK_OUT:
                            onSwipeCheckOut(data);
                            break;
                    }
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0){
                        if(isSupportedSwipeableCheckOut()) {
                            p.setColor(Color.parseColor("#D32F2F"));
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_clear_white_24dp);
                            RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        }
                    } else {
                        if(isSupportedSwipeableCheckIn()) {
                            p.setColor(Color.parseColor("#388E3C"));
                            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_check_circle_white_24dp);
                            RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        }
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecycler);
    }
}
