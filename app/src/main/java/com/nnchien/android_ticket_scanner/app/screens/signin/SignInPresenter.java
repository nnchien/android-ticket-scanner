package com.nnchien.android_ticket_scanner.app.screens.signin;

import android.content.Context;

import com.nnchien.android_ticket_scanner.app.screens.APresenter;
import com.nnchien.android_ticket_scanner.app.screens.main.MainContract;

import java.lang.ref.WeakReference;

/**
 * Created by ngocchien.nguyen on 5/30/17.
 */

public class SignInPresenter extends APresenter<SignInContract.View> implements SignInContract.Presenter {

    public SignInPresenter(SignInContract.View view, Context applicationContext) {
        super(view, applicationContext);
    }
}
