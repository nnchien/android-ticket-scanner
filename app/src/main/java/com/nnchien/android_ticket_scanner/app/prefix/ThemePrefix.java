package com.nnchien.android_ticket_scanner.app.prefix;


import android.support.annotation.IntDef;

import com.nnchien.android_ticket_scanner.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.atomic.AtomicInteger;

import rx.functions.Action1;

import static com.nnchien.android_ticket_scanner.app.prefix.ThemePrefix.Theme.DARK;
import static com.nnchien.android_ticket_scanner.app.prefix.ThemePrefix.Theme.LIGHT;

public class ThemePrefix implements Action1<Integer> {
    @IntDef(flag = true, value = {LIGHT, DARK})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Theme {
        int LIGHT = 0;
        int DARK = 1;
    }

    private static final ThemePrefix INSTANCE = new ThemePrefix();

    private final AtomicInteger currentTheme = new AtomicInteger(LIGHT);

    private ThemePrefix() {
        super();
    }

    public static Action1<Integer> asObserver() {
        return INSTANCE;
    }

    public static int getPop() {
        return R.style.AppTheme_PopupOverlay;
    }

    @Override
    public void call(Integer integer) {
        INSTANCE.currentTheme.set(integer);
    }
}
