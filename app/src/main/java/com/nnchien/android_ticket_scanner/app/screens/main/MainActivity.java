package com.nnchien.android_ticket_scanner.app.screens.main;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.nnchien.android_ticket_scanner.R;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestsFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.checkedin.CheckedInFragment;
import com.nnchien.android_ticket_scanner.app.screens.scanner.ScannerActivity;
import com.nnchien.android_ticket_scanner.app.screens.signin.SignInActivity;
import com.nnchien.android_ticket_scanner.app.screens.ATrackedActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class  MainActivity extends ATrackedActivity implements MainContract.View, SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_SCANNER_CODE = 1000;

    private FragmentPagerAdapter mPagerAdapter;
    private MenuItem mSearchMenuItem;
    private SearchView mSearchView;

    @BindView(R.id.container) ViewPager vViewPager;
    @BindView(R.id.tabs) TabLayout vTabLayout;
    @BindView(R.id.fab_new_guest) View vFabAddNewGuest;

    @Inject MainContract.Presenter mPresenter;

    private AListFragment[] mFragments;
    private String[] mFragmentNames;

    public static Intent newInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupTabs();
    }

    private void setupTabs() {
        mFragments = new AListFragment[] {
                new CheckedInFragment(),
                new GuestsFragment(),
        };

        mFragmentNames = new String[] {
                getString(R.string.heading_checked_in),
                getString(R.string.heading_guests),
        };

        // Create the adapter that will return a fragment for each section
        mPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                return mFragments[position];
            }
            @Override
            public int getCount() {
                return mFragments.length;
            }
            @Override
            public CharSequence getPageTitle(int position) {
                return mFragmentNames[position];
            }
        };

        vViewPager.setAdapter(mPagerAdapter);
        vTabLayout.setupWithViewPager(vViewPager);
    }

    @OnClick({R.id.fab_scan, R.id.fab_new_guest})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_scan:
                startActivityForResult(ScannerActivity.newIntent(this), REQUEST_SCANNER_CODE);
                break;
            case R.id.fab_new_guest:
                showAddNewGuestDialog("");
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchMenuItem = menu.findItem(R.id.search);
        mSearchView = (SearchView) mSearchMenuItem.getActionView();

        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_SCANNER_CODE) {
            if(resultCode == RESULT_OK){
                final String key = data.getStringExtra("key");
                showCheckInDialog(key);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, SignInActivity.class));
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        int position = vViewPager.getCurrentItem();
        if (position < 0 || position >= mFragmentNames.length) {
            return false;
        }
        mFragments[position].search(newText);
        return true;
    }

    public void showAddNewGuestDialog(final String key) {
        final AddNewGuestMaterialDialogBuilder builder = new AddNewGuestMaterialDialogBuilder(this, mPresenter.getTables(), key);
        builder
                .title("Add new guest")
                .customView(getLayoutInflater().inflate(R.layout.dialog_add_new_guest, null), true)
                .positiveText("Add")
                .negativeText("Cancel")
                .cancelable(false)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if(builder.isValidInfo()) {
                            dialog.dismiss();
                            showProgressDialog();
                            mPresenter.addGuest(builder.getData());
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void showCheckInDialog(final String key) {
        new MaterialDialog.Builder(this)
                .title("How come?")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .positiveText("Check in")
                .cancelable(false)
                .input("number of guests", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        if(TextUtils.isDigitsOnly(input)) {
                            checkIn(key, Integer.parseInt(input.toString()));
                        } else {
                            Toast.makeText(MainActivity.this, "Please insert number", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).show();
    }

    private void checkIn(String key, int slots) {
        GuestsFragment guestFragment = (GuestsFragment)mFragments[1];
        guestFragment.checkIn(key, slots);
    }

    @Override
    public void onTableLoaded() {
        vFabAddNewGuest.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCheckInForNewGuest(String key, int slots) {
        checkIn(key, slots);
        hideProgressDialog();
    }

    public void onCheckInError(String key) {
        showAddNewGuestDialog(key);
    }
}
