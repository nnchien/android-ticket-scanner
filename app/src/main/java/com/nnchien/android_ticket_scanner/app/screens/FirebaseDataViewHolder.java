package com.nnchien.android_ticket_scanner.app.screens;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.nnchien.android_ticket_scanner.models.FirebaseData;

import butterknife.ButterKnife;

/**
 * Created by ngocchien.nguyen on 6/5/17.
 */

public abstract class FirebaseDataViewHolder<T> extends RecyclerView.ViewHolder {

    protected FirebaseData mData;

    public FirebaseDataViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void bindToPost(T data, View.OnClickListener starClickListener);

    public FirebaseData getData() {
        return mData;
    }
}
