package com.nnchien.android_ticket_scanner.app.screens.main.guests;

import android.content.Context;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nnchien.android_ticket_scanner.app.screens.AFirebasePresenter;
import com.nnchien.android_ticket_scanner.app.screens.main.MainContract;
import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.models.Table;
import com.nnchien.android_ticket_scanner.utils.Base64Util;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

public class GuestsPresenter extends AFirebasePresenter<GuestsContract.View> implements GuestsContract.Presenter {

    public GuestsPresenter(GuestsContract.View view, Context applicationContext) {
        super(view, applicationContext);
    }

    @Override
    public Query search(String searchText) {
        return mDatabase.child("guests")
                .orderByChild("name")
                .startAt(searchText)
                .endAt(searchText + "\uf8ff");
    }

    @Override
    public void checkIn(final String key, final int slots) {
        mDatabase.child("guests").child(key).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Guest guest = Guest.parse(dataSnapshot);
                if(guest == null || TextUtils.isEmpty(guest.getKey())) {
                    if (getView() instanceof GuestsContract.View) {
                        ((GuestsContract.View) getView()).onCheckInError(key);
                    }
                    return;
                }
                guest.setSlots(slots);
                guest.setCheckedIn(System.currentTimeMillis());
                if(getView() instanceof GuestsContract.View) {
                    ((GuestsContract.View)getView()).onCheckInSuccess();
                }
                listenTableToProcess(guest, guest.getTableKey(), slots);
                mDatabase.child("guests").child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(getView() instanceof GuestsContract.View) {
                    ((GuestsContract.View)getView()).onCheckInError(new Exception(databaseError.getMessage() + " " + key));
                }
            }
        });
    }

    private void listenTableToProcess(final Guest guest, final String tableKey, final int slots) {
        mDatabase.child("tables").child(tableKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Table table = Table.parse(dataSnapshot);
                if(!isExisted(table.getGuests(), guest)) {
                    int newSlots = table.getCheckedIn() + slots;
                    mDatabase.child("tables").child(tableKey).updateChildren(addSlots(newSlots));
                    mDatabase.child("tables").child(tableKey).child("guests").child(guest.getKey()).setValue(guest);
                    if (newSlots > table.getCapacity()) {
                        if (getView() instanceof GuestsContract.View) {
                            ((GuestsContract.View) getView()).onTableIsOverLoad(table.getName(), slots, table.getCapacity() - table.getCheckedIn());
                        }
                    }
                }
                mDatabase.child("checks").child(guest.getKey()).setValue(guest);
                mDatabase.child("guests").child(guest.getKey()).removeValue();
                mDatabase.child("tables").child(tableKey).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(getView() instanceof GuestsContract.View) {
                    ((GuestsContract.View) getView()).onCheckInTableError();
                }
            }
        });
    }

    private Map<String, Object> addSlots(int slots) {
        Map<String, Object> result = new HashMap<>();
        result.put("checkedIn", slots);
        return result;
    }

    public boolean isExisted(List<Guest> guests, Guest guest) {
        if(guests == null) {
            return false;
        }
        for(Guest g : guests) {
            if(g.getKey() == guest.getKey()) {
                return true;
            }
        }
        return false;
    }
}
