package com.nnchien.android_ticket_scanner.app.screens;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.nnchien.android_ticket_scanner.R;
import butterknife.ButterKnife;

/**
 * Created by ngocchien.nguyen on 27/04/16.
 */
public abstract class ATrackedActivity extends ARxActivity {

    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    //----------------------------------------------------------------------------------------------
    // Toolbar
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //----------------------------------------------------------------------------------------------
    // Start Activity
    //----------------------------------------------------------------------------------------------

    protected void slideOutFromLeft(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.acitivity_in_from_left_to_right, R.anim.activity_out_from_right);
    }

    protected void slideInFromRight(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.acitivity_in_from_right_to_left, R.anim.acitivity_out_from_left);
    }

    protected void slideOutFromLeftZoom(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.activity_zoom_in, R.anim.activity_out_from_right);
    }

    protected void slideInFromRightZoom(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.acitivity_in_from_right_to_left, R.anim.activity_zoom_out);
    }

    protected void slideInFromLeftZoom(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.acitivity_in_from_left_to_right, R.anim.activity_zoom_out);
    }

    //----------------------------------------------------------------------------------------------
    // Helpers function
    //----------------------------------------------------------------------------------------------

    protected void replaceFragment(final Fragment fg, final int containerResId, final boolean animated) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tx = fm.beginTransaction();
        if (animated) {
            tx.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        }

        tx.replace(containerResId, fg);
        //tx.addToBackStack(LOG_TAG);
        tx.commit();
        fm.executePendingTransactions();
    }

    protected void openUrlInBrowser(String url) {
        if (url == null || url.length() <= 0)
            return;
        Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            intent.setData(Uri.parse(url));
        } catch (Exception e) {
            return;
        }
        startActivity(intent);
    }
}
