package com.nnchien.android_ticket_scanner.app.screens;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by ngocchien.nguyen on 19/05/16.
 */
public abstract class ATrackedFragment extends ARxFragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    //----------------------------------------------------------------------------------------------
    // Configuration
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = setContentView(inflater, container);
        ButterKnife.bind(this, contentView);

        return contentView;
    }

    protected abstract View setContentView(LayoutInflater inflater, ViewGroup container);

}
