package com.nnchien.android_ticket_scanner.app.screens.main.checkedin;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;
import com.nnchien.android_ticket_scanner.R;
import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestViewHolder;

/**
 * Created by ngocchien.nguyen on 7/22/17.
 */

public class CheckedInAdapter extends FirebaseRecyclerAdapter<Guest, GuestViewHolder> {

    public CheckedInAdapter(Query ref) {
        super(Guest.class, R.layout.item_guest, GuestViewHolder.class, ref);
    }

    @Override
    protected void populateViewHolder(GuestViewHolder viewHolder, Guest model, int position) {
        viewHolder.bindToPost(model, null);
    }
}
