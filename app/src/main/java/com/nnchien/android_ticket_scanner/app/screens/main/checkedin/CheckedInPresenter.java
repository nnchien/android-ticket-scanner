package com.nnchien.android_ticket_scanner.app.screens.main.checkedin;

import android.content.Context;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nnchien.android_ticket_scanner.app.screens.AFirebasePresenter;
import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.models.Table;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

public class CheckedInPresenter extends AFirebasePresenter<CheckedInContract.View> implements CheckedInContract.Presenter{

    public CheckedInPresenter(CheckedInContract.View view, Context applicationContext) {
        super(view, applicationContext);
    }

    @Override
    public Query search(String searchText) {
        return mDatabase.child("checks")
                .orderByChild("name")
                .startAt(searchText)
                .endAt(searchText + "\uf8ff");
    }

    @Override
    public void checkOut(final Guest data) {
        mDatabase.child("checks").child(data.getKey()).removeValue();
        final int slots = data.getSlots();
        data.setSlots(0);
        data.setCheckedIn(0);
        mDatabase.child("guests").child(data.getKey()).setValue(data);
        mDatabase.child("tables").child(data.getTableKey()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Table table = Table.parse(dataSnapshot);
                    int newSlots = table.getCheckedIn() - slots;
                    if (newSlots >= 0) {
                        mDatabase.child("tables").child(data.getTableKey()).updateChildren(updateCheckInValue(newSlots));
                        mDatabase.child("tables").child(data.getTableKey()).child("guests").child(data.getKey()).removeValue();
                    }
                    mDatabase.child("tables").child(data.getTableKey()).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private Map<String, Object> updateCheckInValue(int value) {
        Map<String, Object> result = new HashMap<>();
        result.put("checkedIn", value);
        return result;
    }
}
