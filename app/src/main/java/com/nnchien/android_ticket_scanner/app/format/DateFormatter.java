package com.nnchien.android_ticket_scanner.app.format;

import android.text.format.DateFormat;

import java.util.Calendar;

/**
 * Created by ngocchien.nguyen on 8/5/17.
 */

public class DateFormatter {

    public static CharSequence formatDate(long milliseconds) {
        return new DateFormat().format("dd-MM-yyyy", milliseconds);
    }

    public static CharSequence formatDateTime(long milliseconds) {
        return new DateFormat().format("dd-MM-yyyy-HH-mm-ss", milliseconds);
    }

    public static String formatterMillisecond(long milliseconds) {
        return new DateFormat().format("dd-MM-yyyy HH:mm:ss", milliseconds).toString();
    }

    public static long dateFormatter(int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        return calendar.getTimeInMillis();
    }
}
