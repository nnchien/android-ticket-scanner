package com.nnchien.android_ticket_scanner.app.screens.main.guests;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnchien.android_ticket_scanner.R;
import com.nnchien.android_ticket_scanner.app.screens.FirebaseDataViewHolder;
import com.nnchien.android_ticket_scanner.models.Guest;

import butterknife.BindView;

import static com.nnchien.android_ticket_scanner.utils.DisplayUtils.displaySideIcon;

public class GuestViewHolder extends FirebaseDataViewHolder<Guest> {

    @BindView(R.id.side_title) TextView sideView;
    @BindView(R.id.side_icon) ImageView iconView;
    @BindView(R.id.star) ImageView starView;
    @BindView(R.id.slots) TextView numStarsView;
    @BindView(R.id.table_title) TextView table;

    public GuestViewHolder(View itemView) {
        super(itemView);
    }

    public void bindToPost(Guest data, View.OnClickListener starClickListener) {
        mData = data;
        sideView.setText(data.getName());
        table.setText(data.getTableName());
        iconView.setImageResource(displaySideIcon(data.getSide()));
        numStarsView.setText(String.valueOf(data.getSlots()));
        starView.setOnClickListener(starClickListener);
        if (data.getCheckedIn() > 0) {
            starView.setImageResource(R.drawable.ic_toggle_star_24);
        } else {
            starView.setImageResource(R.drawable.ic_toggle_star_outline_24);
        }
    }
}
