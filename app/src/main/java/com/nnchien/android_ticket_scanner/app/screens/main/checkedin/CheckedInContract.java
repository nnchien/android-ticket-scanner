package com.nnchien.android_ticket_scanner.app.screens.main.checkedin;

import com.google.firebase.database.Query;
import com.nnchien.android_ticket_scanner.models.Guest;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

public interface CheckedInContract {

    interface View {

    }

    interface Presenter {

        Query search(String searchText);

        void checkOut(Guest data);
    }
}
