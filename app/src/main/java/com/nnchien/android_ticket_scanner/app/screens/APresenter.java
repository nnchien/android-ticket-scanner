package com.nnchien.android_ticket_scanner.app.screens;

import android.content.Context;

import com.nnchien.android_ticket_scanner.app.screens.main.MainContract;

import java.lang.ref.WeakReference;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

public abstract class APresenter<T> {

    private WeakReference<T> mViewWeakReference;
    private Context mApplicationContext;

    public APresenter(T view, Context applicationContext) {
        mViewWeakReference = new WeakReference<>(view);
        mApplicationContext = applicationContext;
    }

    protected T getView() {
        return mViewWeakReference.get();
    }

    protected Context getContext() {
        return mApplicationContext;
    }
}
