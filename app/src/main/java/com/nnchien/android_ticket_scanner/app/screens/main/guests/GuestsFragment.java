package com.nnchien.android_ticket_scanner.app.screens.main.guests;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.nnchien.android_ticket_scanner.app.screens.ATrackedActivity;
import com.nnchien.android_ticket_scanner.app.screens.main.AListFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.MainActivity;
import com.nnchien.android_ticket_scanner.models.FirebaseData;
import com.nnchien.android_ticket_scanner.app.screens.FirebaseDataViewHolder;
import com.nnchien.android_ticket_scanner.models.Guest;

import javax.inject.Inject;

import static android.content.Context.CLIPBOARD_SERVICE;

public class GuestsFragment extends AListFragment implements GuestsContract.View {

    @Inject
    GuestsContract.Presenter mPresenter;

    public GuestsFragment() {
    }

    @Override
    protected FirebaseRecyclerAdapter<? extends FirebaseData, ? extends FirebaseDataViewHolder> getAdapter(String searchText) {
        return new GuestsAdapter(mPresenter.search(searchText));
    }

    public void checkIn(String key, int slots) {
        mPresenter.checkIn(key, slots);
        if (getActivity() instanceof ATrackedActivity) {
            ((ATrackedActivity) getActivity()).showProgressDialog();
        }
    }

    @Override
    public void onCheckInError(String key) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity)getActivity()).onCheckInError(key);
        }
    }

    @Override
    public void onCheckInError(Exception e) {
        if(getActivity() instanceof ATrackedActivity) {
            ((ATrackedActivity) getActivity()).hideProgressDialog();
        }
        Toast.makeText(getContext(), "err: cannot find user for key " + e.getMessage(), Toast.LENGTH_SHORT).show();
        ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", e.getMessage());
        clipboard.setPrimaryClip(clip);
    }

    @Override
    public void onCheckInSuccess() {
        if(getActivity() instanceof ATrackedActivity) {
            ((ATrackedActivity) getActivity()).hideProgressDialog();
        }
        Toast.makeText(getContext(), "success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTableIsOverLoad(String tableName, int requested, int slotsLeft) {
        Toast.makeText(getContext(), String.format("cannot check-in to table: %s, requested: %d, but have %d", tableName, requested, slotsLeft), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckInTableError() {

    }

    @Override
    protected void onSwipeCheckIn(Guest data) {
        Toast.makeText(getContext(), "check-in for " + data.getName(), Toast.LENGTH_SHORT).show();
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showCheckInDialog(data.getKey());
        }
    }

    @Override
    protected boolean isSupportedSwipeableCheckIn() {
        return true;
    }
}
