package com.nnchien.android_ticket_scanner.app.screens.scanner;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.nnchien.android_ticket_scanner.app.screens.ATrackedActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScannerActivity extends ATrackedActivity implements ZBarScannerView.ResultHandler, ScannerContract.View {

    private static final int REQUEST_CAMERA_PERMISSION = 1000;
    private static final String TAG = ScannerActivity.class.getSimpleName();

    private ZBarScannerView mScannerView;

    public static Intent newIntent(Activity activity) {
        return new Intent(activity, ScannerActivity.class);
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        } else {
            setupScreen();
        }
    }

    private void setupScreen() {
        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupScreen();
                } else {
                    finish();
                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mScannerView != null) {
            mScannerView.stopCamera();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        String key = rawResult.getContents();
        if(!TextUtils.isEmpty(key)) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("key",key);
            setResult(RESULT_OK, returnIntent);
            finish();
            return;
        }
        Toast.makeText(getBaseContext(), "wrong code", Toast.LENGTH_SHORT).show();
    }
}