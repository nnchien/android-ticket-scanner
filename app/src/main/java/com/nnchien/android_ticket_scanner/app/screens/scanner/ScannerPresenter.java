package com.nnchien.android_ticket_scanner.app.screens.scanner;

import android.content.Context;

import com.nnchien.android_ticket_scanner.app.screens.APresenter;

/**
 * Created by ngocchien.nguyen on 5/30/17.
 */

public class ScannerPresenter extends APresenter<ScannerContract.View> implements ScannerContract.Presenter {

    public ScannerPresenter(ScannerContract.View view, Context applicationContext) {
        super(view, applicationContext);
    }
}
