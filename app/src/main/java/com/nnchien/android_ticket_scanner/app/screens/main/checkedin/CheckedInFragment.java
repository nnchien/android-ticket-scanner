package com.nnchien.android_ticket_scanner.app.screens.main.checkedin;

import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.nnchien.android_ticket_scanner.app.screens.main.AListFragment;
import com.nnchien.android_ticket_scanner.models.FirebaseData;
import com.nnchien.android_ticket_scanner.app.screens.FirebaseDataViewHolder;
import com.nnchien.android_ticket_scanner.models.Guest;

import javax.inject.Inject;

public class CheckedInFragment extends AListFragment implements CheckedInContract.View {

    @Inject CheckedInContract.Presenter mPresenter;

    public CheckedInFragment() {}

    @Override
    protected FirebaseRecyclerAdapter<? extends FirebaseData, ? extends FirebaseDataViewHolder> getAdapter(String searchText) {
        return new CheckedInAdapter(mPresenter.search(searchText));
    }

    @Override
    protected void onSwipeCheckOut(Guest data) {
        Toast.makeText(getContext(), "check-out for " + data.getName(), Toast.LENGTH_SHORT).show();
        mPresenter.checkOut(data);
    }

    @Override
    protected boolean isSupportedSwipeableCheckOut() {
        return true;
    }
}
