package com.nnchien.android_ticket_scanner.app.screens.main;

import android.content.Context;
import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.nnchien.android_ticket_scanner.app.screens.AFirebasePresenter;
import com.nnchien.android_ticket_scanner.app.screens.APresenter;
import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.models.Table;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ngocchien.nguyen on 5/25/17.
 */

public class MainPresenter extends AFirebasePresenter<MainContract.View> implements MainContract.Presenter {

    private static Map<String, Table> sTables;
    private StatusListener mStatusListener;

    public MainPresenter(MainContract.View view, Context applicationContext) {
        super(view, applicationContext);
        mStatusListener = new StatusListener(this);
        mDatabase.child("tables").addValueEventListener(mStatusListener);
    }

    @Override
    public Map<String, Table> getTables() {
        return sTables;
    }

    @Override
    public void addGuest(final Guest guest) {
        mDatabase.child("guests").child(guest.getKey()).setValue(guest);
        mDatabase.child("guests").child(guest.getKey()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Guest g = Guest.parse(dataSnapshot);
                if(g != null && g.getKey().equals(guest.getKey())) {
                    ((MainContract.View)getView()).onCheckInForNewGuest(guest.getKey(), guest.getSlots());
                }
                mDatabase.child("guests").child(guest.getKey()).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setTables(Map<String, Table> tables) {
        sTables = tables;
        mDatabase.child("tables").removeEventListener(mStatusListener);
        if(tables != null && tables.size() > 0){
            ((MainContract.View)getView()).onTableLoaded();
        }
    }

    private static class StatusListener implements ValueEventListener {

        private WeakReference<MainPresenter> weakReference;

        private StatusListener(MainPresenter presenter) {
            weakReference = new WeakReference<>(presenter);
        }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Map<String, Table> tables = getStatus(dataSnapshot);
            if(weakReference != null && weakReference.get() != null) {
                weakReference.get().setTables(tables);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }

        private Map<String, Table> getStatus(DataSnapshot dataSnapshot) {
            Map<String, Table> tables = new HashMap<>();
            for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                Table table = Table.parse(messageSnapshot);
                if(table != null && !TextUtils.isEmpty(table.getKey()) && !TextUtils.isEmpty(table.getName())) {
                    tables.put(table.getName(), table);
                }
            }
            return tables;
        }
    }
}
