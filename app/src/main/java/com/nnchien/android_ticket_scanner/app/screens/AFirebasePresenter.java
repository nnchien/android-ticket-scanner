package com.nnchien.android_ticket_scanner.app.screens;

import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public abstract class AFirebasePresenter<T> extends APresenter {

    protected DatabaseReference mDatabase;

    public AFirebasePresenter(T view, Context applicationContext) {
        super(view, applicationContext);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }
}
