package com.nnchien.android_ticket_scanner.app.screens.main.guests;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

public interface GuestsContract {

    interface View {
        void onCheckInError(String key);

        void onCheckInError(Exception e);

        void onCheckInSuccess();

        void onTableIsOverLoad(String tableName, int requested, int slotsLeft);

        void onCheckInTableError();
    }

    interface Presenter {

        Query search(String searchText);

        void checkIn(String key, int slots);
    }
}
