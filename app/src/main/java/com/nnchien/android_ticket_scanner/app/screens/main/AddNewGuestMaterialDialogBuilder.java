package com.nnchien.android_ticket_scanner.app.screens.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nnchien.android_ticket_scanner.R;
import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.models.Table;
import com.nnchien.android_ticket_scanner.utils.Base64Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ngocchien.nguyen on 9/22/17.
 */

public class AddNewGuestMaterialDialogBuilder extends MaterialDialog.Builder {

    @BindView(R.id.new_name)
    TextView vName;
    @BindView(R.id.new_auto_complete)
    AutoCompleteTextView vTables;
    @BindView(R.id.new_slots)
    TextView vSlots;
    @BindView(R.id.new_key)
    TextView vKey;

    private Map<String, Table> mTable;
    private String mKey;

    public AddNewGuestMaterialDialogBuilder(@NonNull Context context, Map<String, Table> tables, String key) {
        super(context);
        mTable = tables;
        mKey = key;
    }

    public Guest getData() {
        Guest guest = new Guest();
        guest.setKey(!TextUtils.isEmpty(mKey)? mKey : Base64Util.autoGenKey());
        guest.setName(vName.getText().toString().trim());
        guest.setSlots(Integer.parseInt(vSlots.getText().toString()));
        Table table = mTable.get(vTables.getText().toString());
        if(table != null) {
            guest.setSide(table.getSide());
            guest.setTableKey(table.getKey());
            guest.setTableName(table.getName());
            guest.setTableZone(table.getZone());
        }
        return guest;
    }

    @Override
    public MaterialDialog.Builder customView(@NonNull View view, boolean wrapInScrollView) {
        ButterKnife.bind(this, view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, convertTables(mTable));
        vTables.setAdapter(adapter);
        vKey.setText(mKey);
        return super.customView(view, wrapInScrollView);
    }

    private String[] convertTables(Map<String, Table> tables) {
        if(tables == null) {
            return new String[0];
        }
        List<String> temp = new ArrayList<>();
        for(Map.Entry<String, Table> entry : tables.entrySet()) {
            temp.add(entry.getKey());
        }
        return temp.toArray(new String[temp.size()]);
    }

    public boolean isValidInfo() {
        boolean isValid = true;
        if(TextUtils.isEmpty(vName.getText().toString().trim())) {
            vName.setError("Name cannot be empty");
            isValid = false;
        }
        if(TextUtils.isEmpty(vTables.getText().toString()) || mTable.get(vTables.getText().toString()) == null) {
            vTables.setError("Table cannot be empty or not available");
            isValid = false;
        }
        if(TextUtils.isEmpty(vSlots.getText().toString().trim()) || !TextUtils.isDigitsOnly(vSlots.getText().toString().trim())) {
            vSlots.setError("Slots cannot be empty and should be a number");
            isValid = false;
        }
        return isValid;
    }
}
