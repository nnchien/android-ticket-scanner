package com.nnchien.android_ticket_scanner.app.screens.main;

import com.nnchien.android_ticket_scanner.models.Guest;
import com.nnchien.android_ticket_scanner.models.Table;

import java.util.Map;

/**
 * Created by ngocchien.nguyen on 5/25/17.
 */

public interface MainContract {

    interface View {
        void onTableLoaded();

        void onCheckInForNewGuest(String key, int slots);
    }

    interface Presenter {
        Map<String, Table> getTables();

        void addGuest(Guest guest);
    }
}
