package com.nnchien.android_ticket_scanner.models;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by ngocchien.nguyen on 6/2/17.
 */
public class Guest implements FirebaseData {

    private String key;
    private String name;
    private String tableKey;
    private String tableName;
    private String tableZone;
    private long checkedIn;
    private int slots;
    private Side side;

    public String getKey() {
        return key;
    }

    public Side getSide() {
        return side;
    }

    public String getName() {
        return name;
    }

    public long getCheckedIn() {
        return checkedIn;
    }

    public int getSlots() {
        return slots;
    }

    public void setCheckedIn(long checkedIn) {
        this.checkedIn = checkedIn;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public void setTableKey(String tableKey) {
        this.tableKey = tableKey;
    }

    public String getTableKey() {
        return tableKey;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getTableZone() {
        return tableZone;
    }

    public void setTableZone(String tableZone) {
        this.tableZone = tableZone;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSide(Side side) {
        this.side = side;
    }

    public static Guest parse(DataSnapshot data) {
        if (!data.exists()) {
            return null;
        }
        Guest result = new Guest();
        for (DataSnapshot child : data.getChildren()) {
            switch (child.getKey()) {
                case "checkedIn":
                    result.checkedIn = child.getValue(Long.class);
                    break;
                case "key":
                    result.key = child.getValue(String.class);
                    break;
                case "name":
                    result.name = child.getValue(String.class);
                    break;
                case "side":
                    String value = child.getValue(String.class);
                    result.side = value.equals(Side.BRIDE.name()) ? Side.BRIDE : Side.GROOM;
                    break;
                case "slots":
                    result.slots = child.getValue(Integer.class);
                    break;
                case "tableKey":
                    result.tableKey = child.getValue(String.class);
                    break;
                case "tableName":
                    result.tableName = child.getValue(String.class);
                    break;
                case "tableZone":
                    result.tableZone = child.getValue(String.class);
                    break;
                default:
                    break;
            }
        }
        return result;
    }
}
