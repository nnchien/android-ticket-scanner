package com.nnchien.android_ticket_scanner.models;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.GenericTypeIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ngocchien.nguyen on 6/2/17.
 */
public class Table implements FirebaseData {


    private String key;
    private String name;
    private String zone;
    private Side side;
    private int checkedIn;
    private int capacity;
    private List<Guest> guests;

    public String getKey() {
        return key;
    }

    public Side getSide() {
        return side;
    }

    public String getName() {
        return name;
    }

    public int getCheckedIn() {
        return checkedIn;
    }

    public int getCapacity() {
        return capacity;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public String getZone() {
        return zone;
    }

    public static Table parse(DataSnapshot data) {
        if (!data.exists()) {
            return null;
        }
        Table result = new Table();
        for (DataSnapshot child : data.getChildren()) {
            switch (child.getKey()) {
                case "checkedIn":
                    result.checkedIn = child.getValue(Integer.class);
                    break;
                case "key":
                    result.key = child.getValue(String.class);
                    break;
                case "name":
                    result.name = child.getValue(String.class);
                    break;
                case "side":
                    String value = child.getValue(String.class);
                    result.side = value.equals(Side.BRIDE.name()) ? Side.BRIDE : Side.GROOM;
                    break;
                case "capacity":
                    result.capacity = child.getValue(Integer.class);
                    break;
                case "guests":
                    GenericTypeIndicator<Map<String, Guest>> g = new GenericTypeIndicator<Map<String, Guest>>() {};
                    result.guests = new ArrayList<>(child.getValue(g).values());
                    break;
                case "zone":
                    result.zone = child.getValue(String.class);
                    break;
                default:
                    break;
            }
        }
        return result;
    }
}
