package com.nnchien.android_ticket_scanner.models;

/**
 * Created by ngocchien.nguyen on 6/2/17.
 */
public enum Side {

    GROOM, BRIDE;
}
