package com.nnchien.android_ticket_scanner.di.screens.main;

import android.app.Application;

import com.nnchien.android_ticket_scanner.app.screens.main.MainActivity;
import com.nnchien.android_ticket_scanner.app.screens.main.MainContract;
import com.nnchien.android_ticket_scanner.app.screens.main.MainPresenter;
import com.nnchien.android_ticket_scanner.di.screens.ActivityScope;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by ngocchien.nguyen on 5/25/17.
 */

@ActivityScope
@Subcomponent(
        modules = {
                MainComponent.MainModule.class
        }
)
public interface MainComponent extends AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {
    }

    @Module
    class MainModule {

        @Provides
        @ActivityScope
        MainContract.View provideMainView(MainActivity view) {
            return view;
        }

        @Provides
        @ActivityScope
        MainContract.Presenter provideMainPresenter(MainContract.View view, Application appContext) {
            return new MainPresenter(view, appContext);
        }
    }
}
