package com.nnchien.android_ticket_scanner.di.screens.signin;

import android.app.Application;

import com.nnchien.android_ticket_scanner.app.screens.main.MainActivity;
import com.nnchien.android_ticket_scanner.app.screens.main.MainContract;
import com.nnchien.android_ticket_scanner.app.screens.main.MainPresenter;
import com.nnchien.android_ticket_scanner.app.screens.signin.SignInActivity;
import com.nnchien.android_ticket_scanner.app.screens.signin.SignInContract;
import com.nnchien.android_ticket_scanner.app.screens.signin.SignInPresenter;
import com.nnchien.android_ticket_scanner.di.screens.ActivityScope;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by ngocchien.nguyen on 5/30/17.
 */

@ActivityScope
@Subcomponent(
        modules = {
                SignInComponent.SignInModule.class
        }
)
public interface SignInComponent extends AndroidInjector<SignInActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SignInActivity> {
    }

    @Module
    class SignInModule {

        @Provides
        @ActivityScope
        SignInContract.View provideMainView(SignInActivity view) {
            return view;
        }

        @Provides
        @ActivityScope
        SignInContract.Presenter provideSignInPresenter(SignInContract.View view, Application appContext) {
            return new SignInPresenter(view, appContext);
        }
    }
}

