package com.nnchien.android_ticket_scanner.di.screens.main.tables;

import android.app.Application;

import com.nnchien.android_ticket_scanner.app.screens.main.checkedin.CheckedInContract;
import com.nnchien.android_ticket_scanner.app.screens.main.checkedin.CheckedInFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.checkedin.CheckedInPresenter;
import com.nnchien.android_ticket_scanner.di.screens.FragmentScope;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

@FragmentScope
@Subcomponent(
        modules = {
                TablesComponent.TablesModule.class
        }
)
public interface TablesComponent extends AndroidInjector<CheckedInFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<CheckedInFragment> {
    }

    @Module
    class TablesModule {

        @Provides
        @FragmentScope
        CheckedInContract.View provideTablesView(CheckedInFragment view) {
            return view;
        }

        @Provides
        @FragmentScope
        CheckedInContract.Presenter provideTablesPresenter(CheckedInContract.View view, Application appContext) {
            return new CheckedInPresenter(view, appContext);
        }
    }
}

