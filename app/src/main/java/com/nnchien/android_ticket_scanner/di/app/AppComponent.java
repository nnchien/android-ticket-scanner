package com.nnchien.android_ticket_scanner.di.app;

import com.nnchien.android_ticket_scanner.App;
import com.nnchien.android_ticket_scanner.di.screens.ScreenBuilderModule;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by ngocchien.nguyen on 27/4/17.
 */
@AppScope
@Component(
        modules = {
                AppModule.class,
                AndroidSupportInjectionModule.class, /* Android support injection */
                ScreenBuilderModule.class,
                IoModule.class
        }
)
public interface AppComponent extends AndroidInjector<App> {

    @dagger.Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {
    }
}
