package com.nnchien.android_ticket_scanner.di.app;

import com.nnchien.android_ticket_scanner.App;
import com.nnchien.android_ticket_scanner.internal.SDKConstants;
import com.nnchien.android_ticket_scanner.internal.db.AppSQLiteOpenHelper;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import dagger.Lazy;
import dagger.Module;
import dagger.Provides;
import rx.Observable;
import rx.functions.Func0;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Module that provides io dependencies
 * Created by ngocchien.nguyen on 5/8/17.
 */

@Module
class IoModule {

    @SuppressWarnings("CheckResult")
    @AppScope
    @Provides
    static BriteDatabase provideBriteDatabase(final App application) {
        final SqlBrite.Builder builder = new SqlBrite.Builder().logger(new SqlBrite.Logger() {
            @Override
            public void log(String message) {
                Timber.d(message);
            }
        });

        final BriteDatabase db = builder.build().wrapDatabaseHelper(new AppSQLiteOpenHelper(application), Schedulers.io());
        db.setLoggingEnabled(SDKConstants.DEVELOPER_MODE);
        return db;
    }

    @AppScope
    @Provides
    static Observable<BriteDatabase> provideBriteDatabaseObservable(final Lazy<BriteDatabase> briteDatabaseLazy) {
        return Observable
                .defer(new Func0<Observable<BriteDatabase>>() {
                    @Override
                    public Observable<BriteDatabase> call() {
                        return Observable.just(briteDatabaseLazy.get());
                    }
                })
                .subscribeOn(Schedulers.io());
    }
}
