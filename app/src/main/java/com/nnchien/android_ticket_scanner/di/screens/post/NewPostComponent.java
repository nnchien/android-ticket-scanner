package com.nnchien.android_ticket_scanner.di.screens.post;

import android.app.Application;

import com.nnchien.android_ticket_scanner.app.screens.scanner.ScannerActivity;
import com.nnchien.android_ticket_scanner.app.screens.scanner.ScannerContract;
import com.nnchien.android_ticket_scanner.app.screens.scanner.ScannerPresenter;
import com.nnchien.android_ticket_scanner.di.screens.ActivityScope;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by ngocchien.nguyen on 5/30/17.
 */

@ActivityScope
@Subcomponent(
        modules = {
                NewPostComponent.NewPostModule.class
        }
)
public interface NewPostComponent extends AndroidInjector<ScannerActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ScannerActivity> {
    }

    @Module
    class NewPostModule {

        @Provides
        @ActivityScope
        ScannerContract.View provideNewPostView(ScannerActivity view) {
            return view;
        }

        @Provides
        @ActivityScope
        ScannerContract.Presenter provideNewPostPresenter(ScannerContract.View view, Application appContext) {
            return new ScannerPresenter(view, appContext);
        }
    }
}

