package com.nnchien.android_ticket_scanner.di.app;

import android.app.Application;

import com.nnchien.android_ticket_scanner.App;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ngocchien.nguyen on 27/4/17.
 */
@Module
class AppModule {

    @Provides
    @AppScope
    Application provideApplication(App application) {
        return application;
    }
}
