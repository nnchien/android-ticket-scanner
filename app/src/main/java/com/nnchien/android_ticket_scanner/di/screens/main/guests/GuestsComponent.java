package com.nnchien.android_ticket_scanner.di.screens.main.guests;

import android.app.Application;

import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestsContract;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestsFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestsPresenter;
import com.nnchien.android_ticket_scanner.di.screens.FragmentScope;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by ngocchien.nguyen on 6/7/17.
 */

@FragmentScope
@Subcomponent(
        modules = {
                GuestsComponent.GuestsModule.class
        }
)
public interface GuestsComponent extends AndroidInjector<GuestsFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<GuestsFragment> {
    }

    @Module
    class GuestsModule {

        @Provides
        @FragmentScope
        GuestsContract.View provideGuestsView(GuestsFragment view) {
            return view;
        }

        @Provides
        @FragmentScope
        GuestsContract.Presenter provideGuestsPresenter(GuestsContract.View view, Application appContext) {
            return new GuestsPresenter(view, appContext);
        }
    }
}

