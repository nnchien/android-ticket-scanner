package com.nnchien.android_ticket_scanner.di.screens;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.nnchien.android_ticket_scanner.app.screens.main.MainActivity;
import com.nnchien.android_ticket_scanner.app.screens.main.guests.GuestsFragment;
import com.nnchien.android_ticket_scanner.app.screens.main.checkedin.CheckedInFragment;
import com.nnchien.android_ticket_scanner.app.screens.scanner.ScannerActivity;
import com.nnchien.android_ticket_scanner.app.screens.signin.SignInActivity;
import com.nnchien.android_ticket_scanner.di.screens.main.MainComponent;
import com.nnchien.android_ticket_scanner.di.screens.main.guests.GuestsComponent;
import com.nnchien.android_ticket_scanner.di.screens.main.tables.TablesComponent;
import com.nnchien.android_ticket_scanner.di.screens.post.NewPostComponent;
import com.nnchien.android_ticket_scanner.di.screens.signin.SignInComponent;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * Created by ngocchien.nguyen on 27/4/17.
 */
@SuppressWarnings("unused")
@Module(subcomponents = {
        MainComponent.class,
        SignInComponent.class,
        NewPostComponent.class,
        GuestsComponent.class,
        TablesComponent.class
})
public abstract class ScreenBuilderModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideMainBuilder(MainComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(SignInActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideSignInBuilder(SignInComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(ScannerActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> provideNewPostBuilder(NewPostComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(GuestsFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> provideGuestsBuilder(GuestsComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(CheckedInFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment> provideTablesBuilder(TablesComponent.Builder builder);

}
