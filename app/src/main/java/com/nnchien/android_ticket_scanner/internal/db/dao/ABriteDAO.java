package com.nnchien.android_ticket_scanner.internal.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.nnchien.android_ticket_scanner.internal.utilities.Preconditions;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Abstract class that contain all the SQLBrite API.
 * <p/>
 * Insert and delete are wrapped into completable.
 * Created by ngocchien.nguyen on 5/8/17.
 */
public abstract class ABriteDAO<T extends ABriteDAO.Valuable> {
    public interface Valuable {
        @NonNull
        ContentValues toContentValues();
    }

    private static final int FAILED_INSERT = -1;

    @NonNull
    protected abstract String getTableName();

    private final Observable<BriteDatabase> database;

    ABriteDAO(@NonNull Observable<BriteDatabase> database) {
        this.database = Preconditions.checkNotNull(database, "BriteDatabase should not be null");
    }

    @SuppressWarnings("ConstantConditions")
    @AnyThread
    Observable<T> doSingleQuery(@NonNull final Func1<Cursor, T> mapper, @NonNull final String sqlStatement, @Nullable final String... sqlArgs) {
        return database.flatMap(new Func1<BriteDatabase, Observable<T>>() {
            @Override
            public Observable<T> call(BriteDatabase briteDatabase) {
                return briteDatabase.createQuery(getTableName(), sqlStatement, sqlArgs)
                        .mapToOneOrDefault(mapper, null);
            }
        });
    }

    @SuppressWarnings("ConstantConditions")
    @AnyThread
    Observable<List<T>> doListQuery(@NonNull final Func1<Cursor, T> mapper, @NonNull final String sqlStatement, @Nullable final String... sqlArgs) {
        return database.flatMap(new Func1<BriteDatabase, Observable<List<T>>>() {
            @Override
            public Observable<List<T>> call(BriteDatabase briteDatabase) {
                return briteDatabase.createQuery(getTableName(), sqlStatement, sqlArgs)
                        .mapToList(mapper);
            }
        });
    }

    @AnyThread
    public Observable<Boolean> insert(@NonNull final T value) {
        Preconditions.checkNotNull(value, "Value should not be null");
        return database
                .map(new Func1<BriteDatabase, Boolean>() {
                    @Override
                    public Boolean call(BriteDatabase briteDatabase) {
                        try {
                            final long result = briteDatabase.insert(getTableName(), value.toContentValues(), getConflictAlgorithm());
                            return result != FAILED_INSERT;
                        } catch (final Exception e) {
                            Timber.w(e);
                            return false;
                        }
                    }
                });
    }

    @AnyThread
    public Observable<Integer> bulkInsert(@NonNull final List<T> values) {
        Preconditions.checkCollections(values, "Values should not be null");

        return database.flatMap(new Func1<BriteDatabase, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(final BriteDatabase briteDatabase) {
                return Observable
                        .fromCallable(new Callable<Integer>() {
                            @Override
                            public Integer call() throws Exception {
                                int successCount = 0;
                                BriteDatabase.Transaction transaction = briteDatabase.newTransaction();
                                try {
                                    for (final T value : values) {
                                        try {
                                            final long result = briteDatabase.insert(getTableName(), value.toContentValues(), getConflictAlgorithm());
                                            if (result != FAILED_INSERT) {
                                                successCount++;
                                            }
                                        } catch (final Exception e) {
                                            Timber.w(e);
                                        }
                                    }
                                    transaction.markSuccessful();
                                } finally {
                                    transaction.end();
                                }

                                return successCount;
                            }
                        });
            }
        });
    }

    @AnyThread
    Observable<Integer> update(@NonNull final T value, @Nullable final String whereClause, @Nullable final String... whereArgs) {
        return update(Preconditions.checkNotNull(value, "Value should not be null").toContentValues(), whereClause, whereArgs);
    }

    @AnyThread
    Observable<Integer> update(@NonNull final ContentValues contentValues, @Nullable final String whereClause, @Nullable final String... whereArgs) {
        Preconditions.checkNotNull(contentValues, "Value should not be null");
        return database.map(new Func1<BriteDatabase, Integer>() {
            @Override
            public Integer call(BriteDatabase briteDatabase) {
                try {
                    return briteDatabase.update(getTableName(), contentValues, whereClause, whereArgs);
                } catch (final Exception e) {
                    return 0;
                }
            }
        });
    }

    @AnyThread
    Observable<Integer> doDelete(@NonNull final String sqlStatement, @NonNull final String... sqlArgs) {
        return database.map(new Func1<BriteDatabase, Integer>() {
            @Override
            public Integer call(BriteDatabase briteDatabase) {
                return briteDatabase.delete(getTableName(), sqlStatement, sqlArgs);
            }
        });
    }

    @AnyThread
    public Observable<Integer> getCount(@NonNull final String whereClause, @NonNull final String... whereArgs) {
        return database.flatMap(new Func1<BriteDatabase, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(BriteDatabase briteDatabase) {
                return briteDatabase.createQuery(getTableName(), "SELECT count(*) AS count FROM " + getTableName() + " " + Preconditions.checkNotNull(whereClause, "Where Clause should not be null"), Preconditions.checkNotNull(whereArgs, "Where args should not be null"))
                        .lift(new Observable.Operator<Integer, SqlBrite.Query>() {
                            @Override
                            public Subscriber<? super SqlBrite.Query> call(final Subscriber<? super Integer> subscriber) {
                                return new Subscriber<SqlBrite.Query>(subscriber) {
                                    @Override
                                    public void onCompleted() {
                                        subscriber.onCompleted();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        subscriber.onError(e);
                                    }

                                    @Override
                                    public void onNext(SqlBrite.Query query) {
                                        final Cursor cursor = query.run();
                                        if (subscriber.isUnsubscribed()) {
                                            return;
                                        }
                                        if (cursor == null) {
                                            subscriber.onNext(0);
                                            return;
                                        }

                                        try {
                                            if (!cursor.moveToFirst() || cursor.getCount() <= 0) {
                                                subscriber.onNext(0);
                                                return;
                                            }

                                            subscriber.onNext(cursor.getInt(0));
                                        } finally {
                                            cursor.close();
                                        }

                                    }
                                };
                            }
                        });
            }
        });
    }

    @AnyThread
    public Observable<Integer> getCount() {
        return database.flatMap(new Func1<BriteDatabase, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(BriteDatabase briteDatabase) {
                return briteDatabase.createQuery(getTableName(), "SELECT count(*) AS count FROM " + getTableName())
                        .lift(new Observable.Operator<Integer, SqlBrite.Query>() {
                            @Override
                            public Subscriber<? super SqlBrite.Query> call(final Subscriber<? super Integer> subscriber) {
                                return new Subscriber<SqlBrite.Query>(subscriber) {
                                    @Override
                                    public void onCompleted() {
                                        subscriber.onCompleted();
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        subscriber.onError(e);
                                    }

                                    @Override
                                    public void onNext(SqlBrite.Query query) {
                                        final Cursor cursor = query.run();
                                        if (subscriber.isUnsubscribed()) {
                                            return;
                                        }
                                        if (cursor == null) {
                                            subscriber.onNext(0);
                                            return;
                                        }

                                        try {
                                            if (!cursor.moveToFirst() || cursor.getCount() <= 0) {
                                                subscriber.onNext(0);
                                                return;
                                            }

                                            subscriber.onNext(cursor.getInt(0));
                                        } finally {
                                            cursor.close();
                                        }

                                    }
                                };
                            }
                        });
            }
        });
    }

    @AnyThread
    public Observable<Integer> clear() {
        return database.map(new Func1<BriteDatabase, Integer>() {
            @Override
            public Integer call(BriteDatabase briteDatabase) {
                return briteDatabase.delete(getTableName(), null);
            }
        });
    }

    @BriteDatabase.ConflictAlgorithm
    @VisibleForTesting
    int getConflictAlgorithm() {
        return SQLiteDatabase.CONFLICT_REPLACE;
    }

    String generateInClause(final int size) {
        if (size == 0) {
            return null;
        }

        if (size == 1) {
            return "IN (?)";
        }
        final StringBuilder builder = new StringBuilder();
        builder.append("IN (");
        for (int i = 0; i < size - 1; i++) {
            builder.append("?,");
        }

        builder.append("?)");
        return builder.toString();
    }
}
