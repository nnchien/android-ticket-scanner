package com.nnchien.android_ticket_scanner.internal;

import com.nnchien.android_ticket_scanner.BuildConfig;

/**
 * Created by ngocchien.nguyen on 7/5/17.
 */

public class SDKConstants {

    public static final boolean DEVELOPER_MODE = BuildConfig.DEBUG;
}
