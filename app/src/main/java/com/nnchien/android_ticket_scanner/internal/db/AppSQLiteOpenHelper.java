package com.nnchien.android_ticket_scanner.internal.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nnchien.android_ticket_scanner.internal.db.entities.AddressTable;

/**
 * Created by ngocchien.nguyen on 5/8/17.
 */
public class AppSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "grabathon.wanderer";
    private static final int DATABASE_VERSION = 1;

    public AppSQLiteOpenHelper(Context context) {
        super(context, AppSQLiteOpenHelper.DATABASE_NAME, null, AppSQLiteOpenHelper.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        AddressTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        AddressTable.onUpgrade(database, oldVersion, newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        AddressTable.onUpgrade(database, oldVersion, newVersion);
    }
}
