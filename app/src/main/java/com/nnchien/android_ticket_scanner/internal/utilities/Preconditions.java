package com.nnchien.android_ticket_scanner.internal.utilities;

import android.support.annotation.Nullable;

import java.util.Collection;

/**
 * Just a copy of useful condition checking from {@link com.google.common.base.Preconditions}
 * Created by ngocchien.nguyen on 5/8/17.
 */

public class Preconditions {
    public static void checkArgument(boolean expression, @Nullable Object errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }

    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        } else {
            return reference;
        }
    }

    public static void checkState(boolean expression, @Nullable Object errorMessage) {
        if (!expression) {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }
    }

    public static <T extends Collection> T checkCollections(T reference, @Nullable Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }

        if (reference.isEmpty()) {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }

        return reference;

    }

    public static <T> T[] checkArrays(T[] reference, @Nullable Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }

        if (reference.length <= 0) {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }

        return reference;
    }
}
