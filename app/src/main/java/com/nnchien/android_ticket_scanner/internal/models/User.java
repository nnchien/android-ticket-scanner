package com.nnchien.android_ticket_scanner.internal.models;

import java.io.Serializable;

/**
 * Created by ngocchien.nguyen on 21/05/16.
 */
public class User implements Serializable {

    private int id;
    private String token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
