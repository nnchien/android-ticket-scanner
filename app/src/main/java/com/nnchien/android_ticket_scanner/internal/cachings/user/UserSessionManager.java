package com.nnchien.android_ticket_scanner.internal.cachings.user;

import com.nnchien.android_ticket_scanner.internal.models.User;

/**
 * Created by ngocchien.nguyen on 17/01/16.
 */
public interface UserSessionManager {

    void clearAllSavedUserData();

    String getCurrentAccessToken();

    void saveCurrentUser(User user);

    User getCurrentUser();

    Number getCurrentUserID();
}
