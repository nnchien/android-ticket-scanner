package com.nnchien.android_ticket_scanner.internal.db.entities;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import timber.log.Timber;

/**
 * Created by ngocchien.nguyen on 5/8/17.
 */
public class AddressTable {
    public static final String TABLE_NAME = "addresses";
    public static final String KEY_CITY = "city";
    public static final String KEY_DISTRICT = "district";
    public static final String KEY_WARD = "ward";
    public static final String KEY_STREET = "street";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_NAME = "name";
    public static final String KEY_LAT = "latitude";
    public static final String KEY_LNG = "longitude";
    public static final String KEY_RECORD_AT = "recordAt";

    public static void onCreate(final SQLiteDatabase database) {
        final StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE ").append(TABLE_NAME);
        builder.append(" (").append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ");
        builder.append(KEY_CITY).append(" TEXT NOT NULL, ");
        builder.append(KEY_DISTRICT).append(" TEXT, ");
        builder.append(KEY_WARD).append(" TEXT, ");
        builder.append(KEY_STREET).append(" TEXT NOT NULL, ");
        builder.append(KEY_NUMBER).append(" TEXT NOT NULL, ");
        builder.append(KEY_NAME).append(" TEXT, ");
        builder.append(KEY_LAT).append(" REAL, ");
        builder.append(KEY_LNG).append(" REAL, ");
        builder.append(KEY_RECORD_AT).append(" INTEGER )");
        database.execSQL(builder.toString());
    }

    public static void onUpgrade(final SQLiteDatabase database, final int oldVersion, final int newVersion) {
        Timber.d("Upgrading database from version %s to %s, which will destroy all old data", oldVersion, newVersion);
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
