package com.nnchien.android_ticket_scanner;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.nnchien.android_ticket_scanner.di.app.DaggerAppComponent;
import com.nnchien.android_ticket_scanner.utils.timber.ThreadNameTree;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ngocchien.nguyen on 5/24/17.
 */

public class App extends DaggerApplication {

    @Override
    protected AndroidInjector<App> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(new ThreadNameTree());
        }

        // Custom font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }
}
