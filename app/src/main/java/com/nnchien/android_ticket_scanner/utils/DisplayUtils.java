package com.nnchien.android_ticket_scanner.utils;

import android.content.Context;

import com.nnchien.android_ticket_scanner.R;
import com.nnchien.android_ticket_scanner.models.Side;

/**
 * Created by ngocchien.nguyen on 6/5/17.
 */

public class DisplayUtils {

    public static String displaySideTitle(Side side, Context context) {
        switch (side) {
            case GROOM:
                return context.getString(R.string.groom);
            case BRIDE:
                return context.getString(R.string.bride);
            default:
                return "unknown";
        }
    }

    public static int displaySideIcon(Side side) {
        switch (side) {
            case GROOM:
                return R.mipmap.ic_groom;
            case BRIDE:
                return R.mipmap.ic_bride;
            default:
                return R.drawable.ic_action_account_circle_40;
        }
    }
}
