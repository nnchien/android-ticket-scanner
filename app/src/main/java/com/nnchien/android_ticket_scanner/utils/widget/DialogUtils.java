package com.nnchien.android_ticket_scanner.utils.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by ngocchien.nguyen on 5/8/17.
 */

public class DialogUtils {

    public static AlertDialog buildAlertDialog(Context context, int titleId, int messId,
                                               DialogInterface.OnClickListener possitiveListener, int possitiveId,
                                               DialogInterface.OnClickListener negativeListener, int negativeId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(titleId));
        builder.setMessage(context.getString(messId));
        builder.setPositiveButton(possitiveId, possitiveListener);
        builder.setNegativeButton(negativeId, negativeListener);
        return builder.create();
    }
}
