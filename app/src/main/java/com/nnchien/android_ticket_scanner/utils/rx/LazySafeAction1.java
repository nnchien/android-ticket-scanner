package com.nnchien.android_ticket_scanner.utils.rx;

import rx.functions.Action1;
import timber.log.Timber;

/**
 * Created by ngocchien.nguyen on 27/4/17.
 */

public abstract class LazySafeAction1<T> implements Action1<T> {

    public abstract void onSafeCall(T t) throws Exception;

    @Override
    public void call(T t) {
        try {
            onSafeCall(t);
        } catch (final Exception e) {
            Timber.d(e);
        }
    }
}
