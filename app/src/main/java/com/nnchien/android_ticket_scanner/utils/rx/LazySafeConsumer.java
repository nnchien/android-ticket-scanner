package com.nnchien.android_ticket_scanner.utils.rx;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Created by ngocchien.nguyen on 8/5/17.
 */

public abstract class LazySafeConsumer<T> implements Consumer<T> {

    public abstract void onSafeAccept(T t) throws Exception;

    @Override
    public void accept(@NonNull T t) throws Exception {
        try {
            onSafeAccept(t);
        } catch (final Exception e) {
            Timber.d(e);
        }
    }
}
