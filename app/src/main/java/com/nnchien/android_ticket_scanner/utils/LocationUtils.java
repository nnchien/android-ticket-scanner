package com.nnchien.android_ticket_scanner.utils;

import android.content.Context;
import android.location.LocationManager;

import org.reactivestreams.Publisher;

import java.util.concurrent.Callable;

import io.reactivex.Flowable;

/**
 * Created by ngocchien.nguyen on 9/5/17.
 */

public class LocationUtils {

    public static Flowable<Boolean> checkGPSIsTurningOn(final Context appContext) {
        return Flowable.defer(new Callable<Publisher<? extends Boolean>>() {
            @Override
            public Publisher<? extends Boolean> call() throws Exception {
                LocationManager lm = (LocationManager) appContext.getSystemService(Context.LOCATION_SERVICE);
                return Flowable.just(lm.isProviderEnabled(LocationManager.GPS_PROVIDER));
            }
        });
    }
}
