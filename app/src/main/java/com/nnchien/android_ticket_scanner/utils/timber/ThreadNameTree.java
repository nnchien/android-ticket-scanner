package com.nnchien.android_ticket_scanner.utils.timber;

import android.text.TextUtils;

import java.util.Locale;

import timber.log.Timber;

/**
 * * Extension of Timber.DebugTree that include thread name in tag.
 * </p>
 * This is useful for tracing threads
 */

public class ThreadNameTree extends Timber.DebugTree {

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + ":" + element.getLineNumber();
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (TextUtils.isEmpty(message)) {
            super.log(priority, tag, message, t);
            return;
        }

        final String threadName = Thread.currentThread().getName();

        // Do not log thread name for main thread!
        // This follows Hugo's scheme which reduce complication on reading!
        if ("MAIN".equalsIgnoreCase(threadName)) {
            super.log(priority, tag, message, t);
            return;
        }

        // This format is Shared with Hugo's scheme. Please don't change it!
        super.log(priority, tag, String.format(Locale.ENGLISH, "%s [%s]", message, threadName), t);
    }
}
