package com.nnchien.android_ticket_scanner.utils.rx;

import android.support.annotation.VisibleForTesting;
import android.util.Log;

import io.reactivex.ObservableOperator;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import rx.Completable;
import rx.CompletableSubscriber;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import timber.log.Timber;

/**
 * Created by ngocchien.nguyen on 3/5/17.
 */

public class Loggable<T> implements ObservableOperator<T, T> {
    private final boolean swallowError;
    final int logLevel;

    public Loggable() {
        this(true, Log.DEBUG);
    }

    public Loggable(int logLevel) {
        this(true, logLevel);
    }

    public Loggable(boolean swallowError) {
        this(swallowError, Log.DEBUG);
    }

    public Loggable(boolean swallowError, int logLevel) {
        this.swallowError = swallowError;
        this.logLevel = logLevel;
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    void log(final int logLevel, final Throwable throwable) {
        Timber.log(logLevel, throwable);
    }

    @Override
    public Observer<? super T> apply(@NonNull final Observer<? super T> observer) throws Exception {
        return new Observer<T>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                observer.onSubscribe(d);
            }

            @Override
            public void onNext(@NonNull T t) {
                observer.onNext(t);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                log(logLevel, e);
                if (swallowError) {
                    onComplete();
                    return;
                }

                observer.onError(e);
            }

            @Override
            public void onComplete() {
                observer.onComplete();
            }
        };
    }

    public Observable.Operator<T, T> forSingle() {
        return new Observable.Operator<T, T>() {
            @Override
            public Subscriber<? super T> call(final Subscriber<? super T> subscriber) {
                return new Subscriber<T>(subscriber) {
                    @Override
                    public void onCompleted() {
                        subscriber.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        log(logLevel, e);
                        if (swallowError) {
                            onNext(null);
                            return;
                        }

                        subscriber.onError(e);
                    }

                    @Override
                    public void onNext(T t) {
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(t);
                            onCompleted();
                        }
                    }
                };
            }
        };
    }

    public Completable.Operator forCompletable() {
        return new Completable.Operator() {
            @Override
            public CompletableSubscriber call(final CompletableSubscriber completableSubscriber) {
                return new CompletableSubscriber() {
                    @Override
                    public void onCompleted() {
                        completableSubscriber.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        log(logLevel, e);
                        if (swallowError) {
                            onCompleted();
                            return;
                        }

                        completableSubscriber.onError(e);
                    }

                    @Override
                    public void onSubscribe(Subscription d) {
                        completableSubscriber.onSubscribe(d);
                    }
                };
            }
        };
    }
}
